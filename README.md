# RabbitMq

#### 介绍
通过thinkPHP5.1实现rabbitMQ，这里就如何安装启用rabbitMq不再做说明，网上有很多教程，请自行安装启用

#### 软件架构
1. rabbitMq实现的基础类：application/common/lib/classes/rabbitmq/RabbitMq.php
2. 供外部调用的rabbitMq类：application/common/lib/classes/RabbitMqWork.php
3. 测试发送消息到rabbitMq中的方法：application/index/controller/Index.php
4. 添加php think命令实现接收rabbitMq中的消息：application/common/command/*.php


#### 使用说明
1. 发送消息时直接在自己的方法中调用RabbitMqWork.php类中的几个送消息的方法即可。
2. application/common/command/下的类都是实现添加php think命令的类，在configure方法中的setName()中设置命令名称，execute()方法是为了执行接收rabbitMq中的消息，同时在application/command.php中return添加设置的命令名称及对应的命令目录地址。

#### 参与贡献

1. [RabbitMQ 中文文档－PHP版](https://xiaoxiami.gitbook.io/rabbitmq_into_chinese_php/)。
2. [RabbitMQ官方文档](https://www.rabbitmq.com/getstarted.html)。
