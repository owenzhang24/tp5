<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

use think\captcha\Captcha;

// 应用公共文件
/**
 * 获取数据库配置
 * @return array|mixed
 */
function getDbConfig($confType = 1)
{
    $configCode = 'changeAdminConf';
    if ($confType != 1) {
        $configCode = 'changeWebConf';
    }
    $isChange = model('SysConfig')->getConfValue($configCode);
    if ($isChange) {
        cache('DB_CONFIG_DATA', null);
        model('SysConfig')->setConfValue($configCode, 0);
    }
    $sysConf = cache('DB_CONFIG_DATA');
    if (!$sysConf) {
        //获取所有系统配置
        $system_config = model('SysConfig')->getDataList(1);
        $sysConf = [];
        foreach ($system_config as $item) {
            $sysConf[$item['configCode']] = $item['configValue'];
        }
        cache('DB_CONFIG_DATA', null);
        cache('DB_CONFIG_DATA', $sysConf, 36000); //缓存配置
    }
    return $sysConf;
}

/**
 *生成验证码
 */
function createCaptcha()
{
    $Captcha = new Captcha();
    $Captcha->length = 4;
    return $Captcha->entry();
}

/**
 * 核对验证码
 */
function checkCaptcha($code)
{
    $Captcha = new Captcha();
    return $Captcha->check($code);
}

/**
 * 生成数据返回格式
 */
function returnData($msg, $status = -1, $data = [])
{
    $rs = ['status' => $status, 'msg' => $msg];
    $rs['data'] = $data;
    return $rs;
}

/**
 * 生成json数据返回格式
 */
function jsonReturn($msg, $status = -1, $data = [])
{
    if (isset($data['status'])) {
        return json_encode($data);
    }
    $rs = ['status' => $status, 'msg' => $msg];
    $rs['data'] = $data ? $data : [];
    return json_encode($rs);
}

//php获取中文字符拼音首字母
function getFirstCharter($str)
{
    if (empty($str)) {
        return '';
    }
    $fchar = ord($str{0});
    if ($fchar >= ord('A') && $fchar <= ord('z')) {
        return strtoupper($str{0});
    }
    $s1 = iconv('UTF-8', 'gb2312', $str);
    $s2 = iconv('gb2312', 'UTF-8', $s1);
    $s = $s2 == $str ? $s1 : $str;
    if (empty($s{1})) {
        return '';
    }
    $asc = ord($s{0}) * 256 + ord($s{1}) - 65536;
    if ($asc >= -20319 && $asc <= -20284) {
        return 'A';
    }
    if ($asc >= -20283 && $asc <= -19776) {
        return 'B';
    }
    if ($asc >= -19775 && $asc <= -19219) {
        return 'C';
    }
    if ($asc >= -19218 && $asc <= -18711) {
        return 'D';
    }
    if ($asc >= -18710 && $asc <= -18527) {
        return 'E';
    }
    if ($asc >= -18526 && $asc <= -18240) {
        return 'F';
    }
    if ($asc >= -18239 && $asc <= -17923) {
        return 'G';
    }
    if ($asc >= -17922 && $asc <= -17418) {
        return 'H';
    }
    if ($asc >= -17417 && $asc <= -16475) {
        return 'J';
    }
    if ($asc >= -16474 && $asc <= -16213) {
        return 'K';
    }
    if ($asc >= -16212 && $asc <= -15641) {
        return 'L';
    }
    if ($asc >= -15640 && $asc <= -15166) {
        return 'M';
    }
    if ($asc >= -15165 && $asc <= -14923) {
        return 'N';
    }
    if ($asc >= -14922 && $asc <= -14915) {
        return 'O';
    }
    if ($asc >= -14914 && $asc <= -14631) {
        return 'P';
    }
    if ($asc >= -14630 && $asc <= -14150) {
        return 'Q';
    }
    if ($asc >= -14149 && $asc <= -14091) {
        return 'R';
    }
    if ($asc >= -14090 && $asc <= -13319) {
        return 'S';
    }
    if ($asc >= -13318 && $asc <= -12839) {
        return 'T';
    }
    if ($asc >= -12838 && $asc <= -12557) {
        return 'W';
    }
    if ($asc >= -12556 && $asc <= -11848) {
        return 'X';
    }
    if ($asc >= -11847 && $asc <= -11056) {
        return 'Y';
    }
    if ($asc >= -11055 && $asc <= -10247) {
        return 'Z';
    }
    return null;
}
