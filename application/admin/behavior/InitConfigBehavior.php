<?php


namespace app\admin\behavior;


use think\Config;
use think\Request;

class InitConfigBehavior
{
    public function run(Request $request, $params)
    {
        config('conf',getDbConfig(GET_ADMIN_CONF));
    }
}