<?php


namespace app\admin\controller;


use think\Controller;

class Base extends Controller
{
    public function __construct(){
        parent::__construct();
        $request = request();
        $this->assign('v',config('conf')['adminVersion']);
//        $this->assign('v','1.0.2');
        $visit = strtolower($request->module()."/".$request->controller()."/".$request->action());
        if(!session('SYS_STAFF') && !in_array($visit,[
                'admin/index/login',
                'admin/index/login.html',
                'admin/index/checklogin',
                'admin/index/checklogin.html',
                'admin/index/getcaptcha',
                'admin/index/getcaptcha.html',
            ]) ){
            header("Location:".url('admin/index/login'));
            exit();
        }
    }

    public function getCaptcha(){
        ob_clean();
        return createCaptcha();
    }
}