<?php
namespace app\admin\controller;


use app\admin\model\Menus;
use app\admin\model\Staffs;

class Index extends Base
{
    public function index()
    {
        $permission = session('SYS_STAFF')['role']['privileges'];
        $this->assign('permission',$permission);
        $Menus = new Menus();
        $this->assign('menus',$Menus->getIndexMenue());
        $staffId = session('SYS_STAFF')['staffId'];
        $Staff = new Staffs();
        $staffInfo = $Staff->getById($staffId);
        $this->assign('staffInfo',$staffInfo);
        return $this->fetch();
    }

    /**
     * 跳去登录页
     */
    public function login(){
        if(session('SYS_STAFF')) {
            header("Location:".url('admin/index/index'));
            die();
        }
        return $this->fetch("/login");
    }

    /**
     * 登录验证
     */
    public function checkLogin(){
        $m = new Staffs();
        $loginName = trim(input('post.loginName/s',''));
        $loginPwd = trim(input('post.loginPwd/s',''));
        $captcha = trim(input('post.captcha/s',''));
        if (empty($loginName))return jsonReturn('未填写登录账号');
        if (empty($loginPwd))return jsonReturn('未填写登录密码');
        if (empty($captcha))return jsonReturn('未填写验证码');
        return $m->checkLogin($loginName,$loginPwd,$captcha);
    }

    /**
     * 退出系统
     */
    public function logout(){
        session('SYS_STAFF',null);
        return returnData("退出成功，正在跳转页面", 1);
    }
}
