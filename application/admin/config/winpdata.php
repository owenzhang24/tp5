<?php
/*所获取配置的地方*/
define("GET_ADMIN_CONF",1);
define("GET_WEB_CONF",2);

return [
    'conf_type' => [//配置类型
        GET_ADMIN_CONF => '后台配置',
        GET_WEB_CONF => '前台配置'
    ],
];