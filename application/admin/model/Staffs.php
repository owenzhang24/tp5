<?php


namespace app\admin\model;

use app\common\model\Staffs as M;
class Staffs extends Base
{

    public function checkLogin($loginName, $loginPwd, $captcha)
    {
        if(!checkCaptcha($captcha)){
            return returnData('验证码错误!');
        }
        $Staff = new M();
        $staff = $Staff->getStaffInfo($loginName,$loginPwd);
        if($staff==false){
            return returnData($Staff->getError(),-1);
        }
        session("SYS_STAFF",$staff);
        return returnData('',1,$staff);
    }

    public function getById($staffId)
    {
        $Staff = new M();
        return $Staff->getById($staffId);
    }
}