<?php


namespace app\admin\model;


class Menus extends Base
{
    /**
     * 获取首页菜单
     * @return array
     */
    public function getIndexMenue(){
        //用户权限判断
        $STAFF = session('SYS_STAFF');
        $datas  = [];
        $menus = $this->alias('m')
            ->join('permission p','m.id= p.menuId','left')
            ->field('m.id,m.name,p.path,m.parentId')
            ->where('p.isMenu',1)
            ->where('m.isShow',1)
            ->where('p.delete',1)
            ->where('m.delete',1)
            ->where([['m.id','in',$STAFF['role']['menuIds']]])
            ->order('sort', 'asc')
            ->select()->toArray();
        if(!empty($menus)){
            foreach($menus as $key =>$v0){
                if($v0['parentId']==0){
                    $v0['child'] = $this->getChildMenus($v0['id'],$menus);
                    $datas[] = $v0;
                }
            }
        }
        return $datas;
    }

    private function getChildMenus($parentId, array $data)
    {
        $rdata = [];
        foreach($data as $v){
            if($v['parentId']==$parentId){
                $v['child'] =  $this->getChildMenus($v['id'],$data);
                $rdata[] = $v;
            }
        }
        return $rdata;
    }
}