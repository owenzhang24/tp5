<?php
/**
 * 博客模块
 */

namespace app\common\model;

use app\common\InstanceTrait;
use think\Db;
use think\Model;

class BlogModel extends Model
{
    use InstanceTrait;

    const BLOG_STATUS_NORMAL = 1; //正常
    const BLOG_STATUS_DELETE = 0; //删除
    const TYPE_EXPERT = 3; //专家文章
    const TYPE_BLOG = 4; //资讯文章
    const TYPE_VIDEO = 5; //视频
    const TYPE_IMG = 6; //资讯图库

    //封面图类型
    const THUMB_TYPE_MIN = 1;//小图
    const THUMB_TYPE_MAX = 2;//大图
    const THUMB_TYPE_THREE = 3;//三图

    //作者类型
    const AUTHOR_TYPE_SYS = 1;//系统作者
    const AUTHOR_TYPE_USER = 2;//用户作者


    //置顶列表
    const TOP_LIST = 1;
    //推送列表
    const PUSH_LIST = 2;
    //普通资讯列表
    const BLOG_LIST = 0;

    const YES = 1;
    const NO = 0;

    //审核状态
    const CHECK_STATUS_NO = 4; //无需审核
    const CHECK_STATUS_ON = 1; //审核通过
    const CHECK_STATUS_OFF = 3; //审核未通过
    const CHECK_STATUS_WAIT = 2; //未审核

    //表名
    const ADMIN_OP_TABLE_NAME = 'blog_instance';

    /**
     * 获取作者的资讯列表分页查询
     */
    public function getAuthorBlogListByPager($authorId, $page, $pageCount, $sys, $typeArr = [4, 5], $authorType = self::AUTHOR_TYPE_SYS)
    {
        
        $op = Db::table('blog_instance')
            ->whereIn('type_id', $typeArr)
            ->where('author_type', '=', $authorType)
            ->where('author_id', $authorId)
            ->where('status', self::BLOG_STATUS_NORMAL)
            ->where('sys', $sys)
            ->field('blog_id,title,thumbnail_type,thumbnail_url,created_at,updated_at,blog_likes_fake,is_recommend,video_url,content')
            ->order('created_at desc');

        //总共记录
        $total = $op->count();
        $list = $op->limit($pageCount * ($page - 1), $pageCount + 1)->select();
        

        $count = count($list);
        $hasMore = false;
        if ($count > $pageCount) {
            $hasMore = true;
            array_pop($list);
        }

        return [
            'data' => $list,
            'total' => $total,
            'has_more' => $hasMore
        ];
    }

    /**
     * 获取资讯列表分页查询
     */
    public function getBlogListByPager(BlogListContext $context, $sys)
    {
        $op = Db::table('blog_instance')
            ->where('sys', $sys)
            ->where('status', self::BLOG_STATUS_NORMAL);
//            ->field('blog_id, type_id, author_id, channel_id, title, created_at, blog_likes_fake, thumbnail_url,video_url,is_recommend,is_stick,blog_likes');

        //类型
        if ($context->type_arr != [-1]) {
            $op = $op->whereIn('type_id', $context->type_arr);
        }

        //作者id
        if ($context->author_id_arr != [-1]) {
            $op = $op->whereIn('author_id', $context->author_id_arr);
        }

        //标题
        if ($context->title) {
            $op = $op->whereLike('title', "%{$context->title}%");
        }
        //时间
        if ($context->start_time && $context->end_time) {
            $op = $op->where('created_at', '<', $context->end_time);
            $op = $op->where('created_at', '>', $context->start_time);
        }

        //根据发布人搜索
        if ($context->adminIdArr) {
            $op = $op->whereIn('admin_id', $context->adminIdArr);
        }

        //频道
        if ($context->channel_id_arr != [-1]) {
            $op = $op->leftJoin('blog_channel_link', 'blog_instance.blog_id = blog_channel_link.blog_id')
                ->whereIn('blog_channel_link.channel_id', $context->channel_id_arr);
        }

        //推送列表 置顶列表
        if ($context->type == self::TOP_LIST) {
            $op = $op->where('is_stick', self::YES);
        } else if ($context->type == self::PUSH_LIST) {
            $op = $op->where('is_push', self::YES);
        }

        //置顶
        if ((string)$context->is_stick != '') {
            $op = $op->where('is_stick', $context->is_stick);
        }

        //是否推送
        if ((string)$context->is_push != '') {
            $op = $op->where('is_push', $context->is_push);
        }
        //审核状态
        if ((string)$context->check_status != '') {
            if ((string)$context->check_status == self::CHECK_STATUS_NO) {
                $op = $op->where('author_type', self::AUTHOR_TYPE_SYS);
            } elseif ((string)$context->check_status == self::CHECK_STATUS_ON) {
                $op = $op->where('check_status', $context->check_status)->where('author_type', self::AUTHOR_TYPE_USER);
            } else {
                $op = $op->where('check_status', $context->check_status);
            }
        }

        $op = $op->order('created_at desc');
        //总共记录
        $total = $op->count();
        $list = $op->limit($context->page_count * ($context->page_no - 1), $context->page_count + 1)->select();

        $count = count($list);
        $hasMore = false;
        if ($count > (int)$context->page_count) {
            $hasMore = true;
            array_pop($list);
        }

        return [
            'data' => array_values($list),
            'total' => $total,
            'has_more' => $hasMore
        ];
    }


    /**
     * 根据id获取一条信息
     */
    public function getOneById($blogId, $type = self::TYPE_BLOG, $sys = '343')
    {
        return DB::table('blog_instance')
            ->where('blog_id', $blogId)
            ->field('blog_id,title,thumbnail_url,content,blog_likes,blog_views,blog_likes_fake,blog_views_fake,type_id,status,author_id,expert_id,is_recommend,is_stick,video_url,created_at')
            ->find();
    }

    /**
     * 根据id获取一条信息
     */
    public function getOneByBlogId($blogId, $sys = '')
    {
        $op = DB::table('blog_instance')
            ->where('blog_id', $blogId);

        if ($sys) {
            $op = $op->where('sys', $sys);
        }

        return $op->find();
    }

    /**
     * 根据idArr和typeIdArr获取详情
     */
    public function getBlogListByIdArrAndTypeIdArr($blogIdArr, $sys = 343, $typeIdArr = [-1])
    {
        $data = Db::table('blog_instance')
            ->whereIn('blog_id', $blogIdArr);

        if ($typeIdArr != [-1]) {
            $data = $data->whereIn('type_id', $typeIdArr);
        }

        $data = $data->select();

        $return = [];
        foreach ($data as $value) {
            $return[$value['blog_id']] = $value;
        }

        return $return;
    }

    /**
     * 专家文章数量
     */
    public function getExpertBlogCount($startTime, $endTime, $sys)
    {
        return Db::table('blog_instance')//专家文章
        ->where('sys', $sys)
            ->where('status', self::BLOG_STATUS_NORMAL)
            ->where('type_id', self::TYPE_EXPERT)
            ->whereBetweenTime('created_at', strtotime($startTime), strtotime($endTime))
            ->count();
    }

    /**
     * 资讯文章数量
     */
    public function getArticleCount($startTime, $endTime, $sys, $typeId = self::TYPE_BLOG)
    {
        return Db::table('blog_instance')
            ->where('sys', $sys)
            ->where('status', self::BLOG_STATUS_NORMAL)
            ->where('type_id', $typeId)
            ->whereBetweenTime('created_at', strtotime($startTime), strtotime($endTime))
            ->count();
    }

    /**
     * 根据adminIdArr和类型获取资讯文章数量
     */
    public function getArticleCountByTypeIdAndAdminId($startTime, $endTime, $sys, $adminId, $typeId = self::TYPE_BLOG)
    {
        return Db::table('blog_instance')
            ->where('sys', $sys)
            ->where('status', self::BLOG_STATUS_NORMAL)
            ->where('type_id', $typeId)
            ->where('admin_id', $adminId)
            ->whereBetweenTime('created_at', strtotime($startTime), strtotime($endTime))
            ->count();
    }

    /**
     * 频道对应的资讯数量
     */
    public function getCountBlogByChannelId($channelId, $type, $sys)
    {
        return DB::table('blog_instance')
            ->leftJoin('blog_channel_link c', 'c.blog_id = blog_instance.blog_id')
            ->where('c.channel_id', $channelId)
            ->where('type_id', $type)
            ->where('status', self::BLOG_STATUS_NORMAL)
            ->where('sys', $sys)
            ->count();
    }

    /**
     * 分页获取所有blog
     */
    public function getAllBlog($page, $size, $sys, $type = 0)
    {
        $list = Db::table('blog_instance')->where('sys', $sys)->where('status', self::BLOG_STATUS_NORMAL);
        if ($type) {
            $list = $list->where('type_id', $type);
        }

        $data = $list->page($page, $size)->select();
        return $data;
    }

    /**
     * 获取所有资讯 临时方法 禁止调用
     */
    public function getAllBlogTmp()
    {
        $list = Db::table('blog_instance')->select();
        return $list;
    }

    /**
     * 修改信息
     */
    public function updateBlog($blogId, $data, $sys)
    {
        return DB::table('blog_instance')
            ->where('blog_id', $blogId)
            ->where('sys', $sys)
            ->update($data);
    }

    /**
     * 分页获取所有blog
     */
    public function deleteBlogByIdArr($blogIdArr, $sys)
    {
        return Db::table('blog_instance')
            ->where('sys', $sys)
            ->whereIn('blog_id', $blogIdArr)
            ->update(['status' => self::BLOG_STATUS_DELETE, 'deleted_at' => time()]);
    }

//获取作者的发文章总数
    public function getBlogCountByAuthorId($authorId, $type, $sys)
    {
        return Db::table('blog_instance')
            ->where('author_id', $authorId)
            ->where('status', self::BLOG_STATUS_NORMAL)
            ->where('type_id', $type)
            ->where('sys', $sys)
            ->count();
    }

    /**
     * description 添加
     */
    public function insertBlogGetId($data)
    {
        return Db::table('blog_instance')->insertGetId($data);
    }

    /**
     * 根据id获取一条信息
     */
    public function getBlogById($blogId, $sys = '343')
    {
        return DB::table('blog_instance')
            ->where('blog_id', $blogId)
            ->where('sys', $sys)
            ->find();
    }

    /**
     * 审核文章
     */
    public function blogChecked($data, $blogId, $sys)
    {
        return DB::table('blog_instance')
            ->where('blog_id', $blogId)
            ->where('sys', $sys)
            ->update($data);
    }
    /*
     * 据审核类型获取用户文章列表
     * @param $userId
     * @param array $checkStatus
     * @param int $pageNo
     * @param int $pageCount
     * @param string $sys
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function getUserBlogListByCheckStatus($userId, $checkStatus = [], $pageNo = 1, $pageCount = 10, $sys = 'sports')
    {
        $op = Db::table('blog_instance')
            ->where('sys', $sys)
            ->where('status', self::BLOG_STATUS_NORMAL)
            ->where('author_type', self::AUTHOR_TYPE_USER)
            ->where('author_id', $userId)
            ->field('blog_instance.blog_id, type_id, author_type, author_id, title, created_at, blog_likes_fake, thumbnail_type,thumbnail_url,video_url,is_recommend,is_stick,blog_views,check_status');

        //如果
        if (!empty($checkStatus)) {
            $op->where('check_status', 'in', $checkStatus);
        }
        //根据创建时间倒序
        $op = $op
            ->order('created_at desc');
        //总共记录
        $total = $op->count();
        $list = $op->limit($pageCount * ($pageNo - 1), $pageCount + 1)->select();

        $count = count($list);
        $hasMore = false;
        if ($count > (int)$pageCount) {
            $hasMore = true;
            array_pop($list);
        }

        return [
            'data' => $list,
            'total' => $total,
            'has_more' => $hasMore
        ];
    }
    //获取未审核文章数量
    public function getCountBlogByCheckStatus($sys)
    {
        $res = Db::table('blog_instance')
        ->where('sys', $sys)
        ->where('check_status', self::CHECK_STATUS_WAIT)
        ->where('type_id', self::TYPE_BLOG)
        ->where('author_type', self::AUTHOR_TYPE_USER)
        ->where('status', self::BLOG_STATUS_NORMAL)
        ->count('check_status');
        return $res;
    }
    //获取未审核视频数量
    public function getCountVideoByCheckStatus($sys)
    {
        $res = Db::table('blog_instance')
        ->where('sys', $sys)
        ->where('check_status', self::CHECK_STATUS_WAIT)
        ->where('type_id', self::TYPE_VIDEO)
        ->where('author_type', self::AUTHOR_TYPE_USER)
        ->where('status', self::BLOG_STATUS_NORMAL)
        ->count('check_status');
        return $res;
    }
    //获取用户发布资讯数量
    public function getCountByUserId($authorId, $sys)
    {
        return Db::table('blog_instance')
        ->where('author_id', $authorId)
        ->where('author_type', self::AUTHOR_TYPE_USER)
        ->where('status', self::BLOG_STATUS_NORMAL)
        ->where('sys', $sys)
        ->count();
    }
}
