<?php
/**
 * 同步资讯
 */

namespace app\polymerize\tool\module\es;

use app\common\model\BlogModel;
use app\common\model\ElasticSearchModel;
use app\polymerize\content\service\BlogService;

class SyncBlog
{
    use \app\common\InstanceTrait;

    public function syncBlog($id)
    {
        //获取用户数据
        $blogInfo = BlogModel::getInstance()->getOneById($id, 4, 'sports');
        if (empty($blogInfo)) {
            return ['code' => -1, 'msg' => '不存在该资讯'];
        }

        //判断是否存在
        $result = ElasticSearchModel::getInstance('sports_search_blog')->getDocById('blog', $id);
var_dump($result);
die();
        if (!empty($result['code'])) {
            return $result;
        }

        //不存在写入
        if (empty($result['data'])) {
            $data = [
                'id' => $id,
                'search' => $blogInfo['title'],
                'status' => $blogInfo['status']
            ];
            ElasticSearchModel::getInstance('sports_search_blog')->addDoc('blog', $data);
            return $result;
        }

        //存在更新
        $data = [
            'search' => $blogInfo['title'],
            'status' => $blogInfo['status']
        ];
        $result = ElasticSearchModel::getInstance('sports_search_blog')->updateDoc('blog', $id, $data);
        return $result;
    }
}
