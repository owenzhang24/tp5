<?php
/**
 * 搜索资讯
 */
namespace app\polymerize\tool\module\es;

use app\common\InstanceTrait;
use app\common\model\BlogModel;
use app\common\model\ElasticSearchModel;

class SearchBlog
{
    use InstanceTrait;

    public function searchBlog($name, $pageNo, $pageCount)
    {
        $data = [
            'query' => [
                'bool' => [
                    'must' => [
                        'match' => [
                            'search' => $name
                        ]
                    ],
                    'filter' => [
                        'term' => [
                            'status' => 1 //TODO 改成常量
                        ]
                    ],
                ],
            ]
        ];
        $result = ElasticSearchModel::getInstance('sports_search_blog')->searchDoc('blog', $data, $pageNo, $pageCount);

        if ($result['code'] != 0 || empty($result['data'])) {
            return $result;
        }

        $data = $result['data'];
        $list = $data['list'];

        $blogIdArr = array_column($list, 'id');
        var_dump($result);
        die();
        $blogInfo = BlogModel::getInstance()->getBlogListByIdArrAndTypeIdArr($blogIdArr);

        $return = [];
        foreach ($list as $value) {
            if (!isset($blogInfo[$value['id']])) {
                continue;
            }

            $return[] = [
                'blog_id' => $value['id'],
                'blog_title' => $blogInfo[$value['id']]['title'],
                'blog_img' => $blogInfo[$value['id']]['thumbnail_url'],
            ];
        }

        $return = [
            'list' => $return,
        ];

        return ['code' => 0, 'data' => $return];
    }
}
